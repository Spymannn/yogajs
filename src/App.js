import React, { Component } from 'react';
import './App.css';
import { Route, Switch , BrowserRouter} from "react-router-dom";

import HomePage from './containters/HomePage/HomePage';
import ChatbotPage from './containters/ChatbotPage/ChatbotPage';


class App extends Component {
  render() {
    return (
      <div className="App">
         

          
          <BrowserRouter>
            <content>
                {/* <Header /> */}
                {/* <Menu logoutHandler={this.logoutHandler.bind(this)}/> */}
                <Switch>
                  <Route path='/' exact component={HomePage} />
                  <Route path='/home' exact component={HomePage} />
                  <Route path='/chatbot' exact component={ChatbotPage} />
                </Switch> 
                {/* <Footer /> */}
            </content> 
          </BrowserRouter>     

         
      </div>
    );
  }
}

export default App;
