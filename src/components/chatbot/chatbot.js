import React from 'react';
import PropTypes from 'prop-types';

import './chatbot.css';
import ChatBot from 'react-simple-chatbot';

const steps = [
    {
      id: '0',
      message: 'Welcome the yoga app!',
      trigger: '1',
    },
    {
      id: '1',
      message: 'What is your name?',
      trigger: '2',
    },
    {
      id: '2',
      user: true,
      trigger: '3',
    },
    {
      id: '3',
      message: 'Hi {previousValue}, nice to meet you!',
      trigger: '4'
    },
    {
      id: '4',
      message: 'Do you want to do some yoga today?',
      trigger: '5'
    },
    {
      id: '5',
      options: [
        { value: 1, label: 'Yes', trigger: '6' },
        { value: 2, label: 'No', trigger: '7' },
      ],
    },
    {
      id: '6',
      message: 'Great, let s begin!',
      end: true,
    },
    {
      id: '7',
      message: 'Too bad, have a great day!',
      end: true,
    },
    
  ];

const Chatbot = (props) => (
    <ChatBot steps={steps} />
)

Chatbot.propTypes = {
    steps: PropTypes.array,
}

export default Chatbot;