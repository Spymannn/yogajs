import React, { Component } from 'react';
import Chatbot from './../../components/chatbot/chatbot';

class ChatbotPage extends Component {
    render() {
        return (
            <Chatbot /> 
        );
    }
}

export default ChatbotPage;