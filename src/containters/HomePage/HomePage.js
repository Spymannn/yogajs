import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SplitText from 'react-pose-text';
import './HomePage.css';
// import logo from './../../assets/img/yoga.png';
import music from './../../assets/sounds/01.mp3';
import Sound from 'react-sound';


const charPoses = {
    exit: { y: 30, opacity: 0 },
    enter: {
      y: 0,
      opacity: 1,
      transition: ({ charInWordIndex }) => ({
        type: 'spring',
        delay: charInWordIndex * 30,
        stiffness: 500 + charInWordIndex * 150,
        damping: 10 - charInWordIndex * 1
      })
    }
  };

  
class HomePage extends Component {
    render() {
        return (
            <div className="header-content">
             <Sound
                url={music}
                playStatus={'PLAYING'}
            />
                {/* <img src={logo} className="App-logo" alt="logo" /> */}
                <h1 className="header-title">Your personal yoga app</h1>
                <h3 className="header-subtitle">A useful start to feel great</h3>
                <Link to={`/chatbot`} className="link">
                    <SplitText initialPose="exit" pose="enter" charPoses={charPoses} className="link-split-text">
                        Enter
                    </SplitText>
                </Link>
            </div>
  
        );
    }
}

export default HomePage;
